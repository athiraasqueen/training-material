# ICFOSS Training Activities Repository
This repository contains reference materials for the various training activities conducted by the International Centre for Free and Open Source Software (ICFOSS) in Kerala, India. ICFOSS is an autonomous organization established by the Government of Kerala with the objective of popularizing Free and Open Source Software (FOSS) for universal use, consolidating past FOSS initiatives in Kerala, and fostering collaboration with different nations, communities, and governments to promote FOSS.


## About ICFOSS

ICFOSS is dedicated to advancing the adoption and awareness of FOSS across different sectors. As an autonomous organization, ICFOSS engages in the following activities:

Promoting FOSS: ICFOSS actively advocates for the use of FOSS and aims to increase its adoption across various domains, including government, education and more.

Training and Capacity Building: ICFOSS conducts training programs and capacity-building initiatives to equip individuals and organizations with the necessary knowledge and skills related to FOSS technologies, tools, and practices.

Networking and Collaboration: ICFOSS collaborates with different nations, communities, and governments to foster knowledge sharing, best practices, and joint initiatives in the field of FOSS. This includes partnerships with international organizations, universities, and industry stakeholders.

# Purpose of this Repository

This repository serves as a centralized collection of reference materials for the training activities conducted by ICFOSS. The repository includes a variety of resources such as presentation slides, code samples, documentation, and other relevant materials pertaining to different training programs.
Contents

The repository is organized into folders based on the specific training programs conducted by ICFOSS

Visit [Our Website](https://icfoss.in/) for more info
